﻿using System;
using System.Threading.Tasks;
using QSales.Core.Model.Abstraction.Interfaces;

namespace QSales.Core.DAL.Abstraction.Interfaces
{
    public interface IPrimaryEntityRepository<in TKey, TEntity> :
        IGenericEntityRepository<TEntity>
        where TEntity : class, ICoreEntity<TKey>
        where TKey : IEquatable<TKey>
    {
        Task<TEntity> GetByIdAsync(TKey id);
        Task<bool> DeleteAsync(TKey id);
        TEntity GetById(TKey id);
        bool Delete(TKey id);
    }
}
