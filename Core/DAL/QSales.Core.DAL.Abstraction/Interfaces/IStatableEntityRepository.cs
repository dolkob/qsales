﻿using System;
using System.Threading.Tasks;
using QSales.Core.Model.Abstraction.Interfaces;

namespace QSales.Core.DAL.Abstraction.Interfaces
{
    public interface IStatableEntityRepository<in TKey, TEntity> :
        IPrimaryEntityRepository<TKey, TEntity>
        where TEntity : class, ICoreEntity<TKey>, IStatable<TKey>
        where TKey : IEquatable<TKey>
    {
        Task<TEntity> ActivateAsync(TEntity entity);
        Task<TEntity> DeactivateAsync(TEntity entity);
        TEntity Deactivate(TEntity entity);
        TEntity Activate(TEntity entity);
    }
}
