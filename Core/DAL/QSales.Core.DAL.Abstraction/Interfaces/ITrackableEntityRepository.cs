﻿using System;
using System.Security.Principal;
using QSales.Core.Model.Abstraction.Interfaces;

namespace QSales.Core.DAL.Abstraction.Interfaces
{
    public interface ITrackableEntityRepository<in TKey, TEntity, TTrackableKey> :
        IStatableEntityRepository<TKey, TEntity>
        where TEntity : class, ITrackable<TTrackableKey, TKey>
        where TKey : IEquatable<TKey>
        where TTrackableKey : IEquatable<TTrackableKey>
    {
        IPrincipal CurrentPrincipal { get; set; }
    }
}
