﻿using System;
using QSales.Core.Model.Abstraction.Interfaces;

namespace QSales.Core.Model.Abstraction.Entities
{
    public abstract class CoreEntity<TKey> : ICoreEntity<TKey>, IStatable<TKey>
        where TKey : IEquatable<TKey>
    {
        public TKey Id { get; set; }
        public bool IsDeleted { get; set; }

        public CoreEntity()
        {
        }

        public CoreEntity(CoreEntity<TKey> coreEntity)
        {
            this.IsDeleted = coreEntity.IsDeleted;
            this.Id = coreEntity.Id;
        }

    }
}
