//using System;

namespace QSales.Core.Model.Abstraction.Entities
{
    public abstract class NameCoreEntity: TrackableEntity<int, int>
    {
        public string Name {get; set;}

        public NameCoreEntity()
        {

        }

        public NameCoreEntity(NameCoreEntity coreEntity)
        {
            this.IsDeleted = coreEntity.IsDeleted;
            this.Id = coreEntity.Id;
            this.Name = coreEntity.Name;
        }
    }
}