﻿using System;
using QSales.Core.Model.Abstraction.Interfaces;

namespace QSales.Core.Model.Abstraction.Entities
{
    public class TrackableEntity<TKey, Key> : CoreEntity<Key>, ITrackable<TKey, Key>
        where TKey : IEquatable<TKey>
        where Key : IEquatable<Key>
    {
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public TKey CreatedBy { get; set; }
        public TKey ModifiedBy { get; set; }
    }
}
