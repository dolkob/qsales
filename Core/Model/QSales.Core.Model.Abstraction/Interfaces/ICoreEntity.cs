﻿using System;

namespace QSales.Core.Model.Abstraction.Interfaces
{
    public interface ICoreEntity<TKey> where TKey : IEquatable<TKey>
    {
        TKey Id { get; set; }
    }
}
