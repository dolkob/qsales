﻿using System;

namespace QSales.Core.Model.Abstraction.Interfaces
{
    public interface IStatable<TKey> : ICoreEntity<TKey>
        where TKey : IEquatable<TKey>
    {
        bool IsDeleted { get; set; }
    }
}
