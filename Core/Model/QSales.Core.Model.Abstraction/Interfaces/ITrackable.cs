﻿using System;

namespace QSales.Core.Model.Abstraction.Interfaces
{
    public interface ITrackable<TrackableKey, Key> : IStatable<Key>
        where TrackableKey : IEquatable<TrackableKey>
        where Key : IEquatable<Key>
    {
        DateTime CreatedDate { get; set; }
        DateTime ModifiedDate { get; set; }
        TrackableKey CreatedBy { get; set; }
        TrackableKey ModifiedBy { get; set; }
    }
}
