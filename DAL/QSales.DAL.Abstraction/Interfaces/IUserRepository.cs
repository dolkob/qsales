﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using QSales.Core.DAL.Abstraction.Interfaces;
using QSales.Model.Identity;

namespace QSales.DAL.Abstraction.Interfaces
{
    public interface IUserRepository : ITrackableEntityRepository<int, QSalesUser, int>
    {
        Task<QSalesUser> Update(QSalesUser entity, string role);
        Task<QSalesUser> Create(QSalesUser entity, string role);
        Task<IList<string>> GetRolesByUserName(string userName);
        Task<string> GetTopRole(QSalesUser user);
    }
}
