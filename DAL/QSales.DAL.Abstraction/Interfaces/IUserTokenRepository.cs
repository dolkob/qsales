using System;
using Microsoft.AspNetCore.Identity;
using QSales.Core.DAL.Abstraction.Interfaces;

namespace QSales.DAL.Abstraction.Interfaces
{
    public interface IUserTokenRepository : IGenericEntityRepository<IdentityUserToken<Guid>>
    {
    
    }
}