﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using QSales.Core.DAL.Abstraction.Interfaces;
using QSales.Model.Context;

namespace QSales.DAL.Repositories.Base
{
    public class GenericEntityRepository<TEntity> :
        IGenericEntityRepository<TEntity>
        where TEntity : class
    {
        protected readonly DbSet<TEntity> DbSet;
        protected readonly DbContext Context;

        public GenericEntityRepository(QSalesContext context)
        {
            Context = context;
            DbSet = context.Set<TEntity>();
        }

        public virtual IQueryable<TEntity> Query => DbSet;

        public virtual IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> predicate = null)
        {
            return predicate != null ? Query.Where(predicate) : Query;
        }

        public virtual async Task<TEntity> GetByIdAsync<TKey>(TKey key)
        {
            return await DbSet.FindAsync(key);
        }

        public virtual TEntity GetById<TKey>(TKey key)
        {
            return DbSet.Find(key);
        }

        protected virtual async Task<TEntity> BeforeCreateAsync(TEntity entity)
        {
            return entity;
        }

        protected virtual async Task<TEntity> AfterCreateAsync(TEntity entity)
        {
            return entity;
        }

        protected virtual async Task<TEntity> DataBaseCreateAsync(TEntity entity)
        {
            var createdEntity = await DbSet.AddAsync(entity);

            return createdEntity.Entity;
        }

        public virtual async Task<TEntity> CreateAsync(TEntity entity)
        {
            entity = await BeforeCreateAsync(entity);

            entity = await DataBaseCreateAsync(entity);

            entity = await AfterCreateAsync(entity);

            return entity;
        }

        protected virtual TEntity BeforeCreate(TEntity entity)
        {
            return entity;
        }

        protected virtual TEntity AfterCreate(TEntity entity)
        {
            return entity;
        }

        protected virtual TEntity DataBaseCreate(TEntity entity)
        {
            entity = DbSet.Add(entity).Entity;

            return entity;
        }

        public virtual TEntity Create(TEntity entity)
        {
            entity = BeforeCreate(entity);

            entity = DataBaseCreate(entity);

            entity = AfterCreate(entity);

            return entity;
        }

        protected virtual TEntity BeforeUpdate(TEntity entity)
        {
            return entity;
        }

        protected virtual TEntity AfterUpdate(TEntity entity)
        {
            return entity;
        }

        protected virtual TEntity DataBaseUpdate(TEntity entity)
        {
            if (Context.Entry(entity).State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }

            Context.Entry(entity).State = EntityState.Modified;

            return entity;
        }

        public virtual TEntity Update(TEntity entity)
        {
            entity = BeforeUpdate(entity);

            entity = DataBaseUpdate(entity);

            entity = AfterUpdate(entity);

            return entity;
        }

        protected virtual async Task<TEntity> BeforeUpdateAsync(TEntity entity)
        {
            return entity;
        }

        protected virtual async Task<TEntity> AfterUpdateAsync(TEntity entity)
        {
            return entity;
        }

        protected virtual async Task<TEntity> DataBaseUpdateAsync(TEntity entity)
        {
            if (Context.Entry(entity).State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }

            Context.Entry(entity).State = EntityState.Modified;

            return entity;
        }

        public virtual async Task<TEntity> UpdateAsync(TEntity entity)
        {
            entity = await BeforeUpdateAsync(entity);

            entity = await DataBaseUpdateAsync(entity);

            entity = await AfterUpdateAsync(entity);

            return entity;
        }

        protected virtual async Task<TEntity> BeforeDeleteAsync(TEntity entity)
        {
            return entity;
        }

        protected virtual async Task<TEntity> AfterDeleteAsync(TEntity entity)
        {
            return entity;
        }

        protected virtual async Task<bool> DataBaseDeleteAsync(TEntity entity)
        {
            try
            {
                if (Context.Entry(entity).State == EntityState.Detached)
                {
                    DbSet.Attach(entity);
                }

                DbSet.Remove(entity);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public virtual async Task<bool> DeleteAsync(TEntity entity)
        {
            entity = await BeforeDeleteAsync(entity);

            var resultDeleting = await DataBaseDeleteAsync(entity);

            await AfterDeleteAsync(entity);

            return resultDeleting;
        }

        protected virtual TEntity BeforeDelete(TEntity entity)
        {
            return entity;
        }

        protected virtual TEntity AfterDelete(TEntity entity)
        {
            return entity;
        }

        protected virtual bool DataBaseDelete(TEntity entity)
        {
            try
            {
                if (Context.Entry(entity).State == EntityState.Detached)
                {
                    DbSet.Attach(entity);
                }

                DbSet.Remove(entity);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public virtual bool Delete(TEntity entity)
        {
            entity = BeforeDelete(entity);

            var resultDeleting = DataBaseDelete(entity);

            AfterDelete(entity);

            return resultDeleting;
        }

        public void Dispose()
        {
            Context.Dispose();
        }

    }
}
