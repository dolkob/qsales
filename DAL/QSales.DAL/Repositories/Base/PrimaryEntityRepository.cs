﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using QSales.Core.DAL.Abstraction.Interfaces;
using QSales.Core.Model.Abstraction.Interfaces;
using QSales.Model.Context;

namespace QSales.DAL.Repositories.Base
{
    public class PrimaryEntityRepository<TKey, TEntity> :
        GenericEntityRepository<TEntity>,
        IPrimaryEntityRepository<TKey, TEntity>
        where TEntity : class, ICoreEntity<TKey>
        where TKey : IEquatable<TKey>
    {
        public PrimaryEntityRepository(QSalesContext context) : base(context)
        {
        }

        public virtual async Task<TEntity> GetByIdAsync(TKey id)
        {
            return await Query.FirstOrDefaultAsync(e => e.Id.Equals(id));
        }

        public async virtual Task<bool> DeleteAsync(TKey id)
        {
            var entity = await GetByIdAsync(id);

            return await DeleteAsync(entity);
        }

        public virtual TEntity GetById(TKey id)
        {
            return Query.FirstOrDefault(e => e.Id.Equals(id));
        }


        public override IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> predicate = null)
        {
            return base.Get(predicate);
        }

        public virtual bool Delete(TKey id)
        {
            var entity = GetById(id);

            return Delete(entity);
        }

    }
}
