﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using QSales.Core.DAL.Abstraction.Interfaces;
using QSales.Core.Model.Abstraction.Interfaces;
using QSales.Model.Context;

namespace QSales.DAL.Repositories.Base
{
    public class StatableEntityRepository<TKey, TEntity> :
        PrimaryEntityRepository<TKey, TEntity>,
        IStatableEntityRepository<TKey, TEntity>
        where TEntity : class, ICoreEntity<TKey>, IStatable<TKey>
        where TKey : IEquatable<TKey>
    {
        public StatableEntityRepository(QSalesContext context)
            : base(context)
        {
        }

        protected override async Task<TEntity> BeforeCreateAsync(TEntity entity)
        {
            entity.IsDeleted = false;

            return await base.BeforeCreateAsync(entity);
        }

        protected override TEntity BeforeCreate(TEntity entity)
        {
            entity.IsDeleted = false;

            return base.BeforeCreate(entity);
        }

        public override IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> predicate = null)
        {
            if (predicate == null)
            {
                return base.Get().Where(x => !x.IsDeleted);
            }
            else
            {
                return base.Get(predicate);
            }
        }

        protected virtual async Task<TEntity> BeforeDeactivateAsync(TEntity entity)
        {
            entity.IsDeleted = true;

            return entity;
        }

        protected virtual async Task<TEntity> AfterDeactivateAsync(TEntity entity)
        {
            return entity;
        }

        protected virtual async Task<TEntity> DataBaseDeactivateAsync(TEntity entity)
        {
            return await UpdateAsync(entity);
        }

        public virtual async Task<TEntity> DeactivateAsync(TEntity entity)
        {
            entity = await BeforeDeactivateAsync(entity);

            entity = await DataBaseDeactivateAsync(entity);

            entity = await AfterDeactivateAsync(entity);

            return entity;
        }

        protected virtual TEntity BeforeDeactivate(TEntity entity)
        {
            entity.IsDeleted = true;

            return entity;
        }

        protected virtual TEntity AfterDeactivate(TEntity entity)
        {
            return entity;
        }

        protected virtual TEntity DataBaseDeactivate(TEntity entity)
        {
            Update(entity);

            return entity;
        }

        public virtual TEntity Deactivate(TEntity entity)
        {
            entity = BeforeDeactivate(entity);

            entity = DataBaseDeactivate(entity);

            entity = AfterDeactivate(entity);

            return entity;
        }

        protected virtual async Task<TEntity> BeforeActivateAsync(TEntity entity)
        {
            entity.IsDeleted = false;

            return entity;
        }

        protected virtual async Task<TEntity> AfterActivateAsync(TEntity entity)
        {
            return entity;
        }

        protected virtual async Task<TEntity> DataBaseActivateAsync(TEntity entity)
        {
            return await UpdateAsync(entity);
        }

        public virtual async Task<TEntity> ActivateAsync(TEntity entity)
        {
            entity = await BeforeActivateAsync(entity);

            entity = await DataBaseActivateAsync(entity);

            entity = await AfterActivateAsync(entity);

            return entity;
        }

        protected virtual TEntity BeforeActivate(TEntity entity)
        {
            entity.IsDeleted = false;

            return entity;
        }

        protected virtual TEntity AfterActivate(TEntity entity)
        {
            return entity;
        }

        protected virtual TEntity DataBaseActivate(TEntity entity)
        {
            return Update(entity);
        }

        public virtual TEntity Activate(TEntity entity)
        {
            entity = BeforeActivate(entity);

            entity = DataBaseActivate(entity);

            entity = AfterActivate(entity);

            return entity;
        }

    }
}
