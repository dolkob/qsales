﻿using System;
using System.Security.Principal;
using System.Threading.Tasks;
using QSales.Core.DAL.Abstraction.Interfaces;
using QSales.Core.Model.Abstraction.Interfaces;
using QSales.Model.Context;

namespace QSales.DAL.Repositories.Base
{
    public class TrackableEntityRepository<TKey, TEntity, TTrackableKey> :
        StatableEntityRepository<TKey, TEntity>,
        ITrackableEntityRepository<TKey, TEntity, TTrackableKey>
        where TEntity : class, ITrackable<TTrackableKey, TKey>
        where TKey : IEquatable<TKey>
        where TTrackableKey : IEquatable<TTrackableKey>
    {
        public IPrincipal CurrentPrincipal { get; set; }

        public TrackableEntityRepository(QSalesContext context, IPrincipal principal)
            : base(context)
        {
            CurrentPrincipal = principal;
        }

        protected override async Task<TEntity> BeforeCreateAsync(TEntity entity)
        {
            entity.CreatedDate = DateTime.UtcNow;
            entity.ModifiedDate = DateTime.UtcNow;
            //entity.CreatedBy = GetCurentUserId();
            //entity.ModifiedBy = GetCurentUserId();

            return await base.BeforeCreateAsync(entity);
        }

        protected override async Task<TEntity> BeforeUpdateAsync(TEntity entity)
        {
            entity.ModifiedDate = DateTime.UtcNow;
            //entity.ModifiedBy = GetCurentUserId();

            return await base.BeforeUpdateAsync(entity);
        }

        protected override async Task<TEntity> BeforeActivateAsync(TEntity entity)
        {
            entity.ModifiedDate = DateTime.UtcNow;
            //entity.ModifiedBy = GetCurentUserId();

            return await base.BeforeActivateAsync(entity);
        }

        protected override async Task<TEntity> BeforeDeactivateAsync(TEntity entity)
        {
            entity.ModifiedDate = DateTime.UtcNow;
            //entity.ModifiedBy = GetCurentUserId();

            return await base.BeforeDeactivateAsync(entity);
        }

        protected override TEntity BeforeCreate(TEntity entity)
        {
            entity.CreatedDate = DateTime.UtcNow;
            entity.ModifiedDate = DateTime.UtcNow;

            //entity.CreatedBy = GetCurentUserId();
            //entity.ModifiedBy = GetCurentUserId();

            return base.BeforeCreate(entity);
        }

        protected override TEntity BeforeUpdate(TEntity entity)
        {
            entity.ModifiedDate = DateTime.UtcNow;
            //entity.ModifiedBy = GetCurentUserId();

            return base.BeforeUpdate(entity);
        }

        protected override TEntity BeforeActivate(TEntity entity)
        {
            entity.ModifiedDate = DateTime.UtcNow;
            //entity.ModifiedBy = GetCurentUserId();

            return base.BeforeActivate(entity);
        }

        protected override TEntity BeforeDeactivate(TEntity entity)
        {
            entity.ModifiedDate = DateTime.UtcNow;
            //entity.ModifiedBy = GetCurentUserId();

            return base.BeforeDeactivate(entity);
        }

    }
}
