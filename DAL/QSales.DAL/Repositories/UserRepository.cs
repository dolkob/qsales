﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using QSales.DAL.Abstraction.Interfaces;
using QSales.DAL.Repositories.Base;
using QSales.Model.Context;
using QSales.Model.Identity;

namespace QSales.DAL.Repositories
{
    public class UserRepository : TrackableEntityRepository<int, QSalesUser, int>, IUserRepository
    {
        private readonly UserManager<QSalesUser> _userManager;

        public UserRepository(UserManager<QSalesUser> userManager, QSalesContext context, IPrincipal principal)
            : base(context, principal)
        {
            _userManager = userManager;
        }

        public async Task<string> GetTopRole(QSalesUser user)
        {
            var roles = await _userManager.GetRolesAsync(user);

            return roles.FirstOrDefault();
        }

        public async Task<IList<string>> GetRolesByUserName(string userName)
        {
            var user = await _userManager.FindByEmailAsync(userName);

            if (user == null) return null;

            var role = await _userManager.GetRolesAsync(user);

            if (role == null) return null;

            return role;
        }


        public async Task<QSalesUser> Update(QSalesUser entity, string role)
        {
            var oldRoles = await _userManager.GetRolesAsync(entity);
            foreach (var r in oldRoles)
            {
                await _userManager.RemoveFromRoleAsync(entity, r);
            }

            await UpdateAsync(entity);

            await _userManager.AddToRoleAsync(entity, role);

            return entity;
        }

        public async Task<QSalesUser> Create(QSalesUser entity, string role)
        {
            var createResult = await _userManager.CreateAsync(entity);

            if (role != null)
            {
                await _userManager.AddToRoleAsync(entity, role);
            }

            entity = await _userManager.FindByEmailAsync(entity.Email);

            return entity;
        }

    }
}
