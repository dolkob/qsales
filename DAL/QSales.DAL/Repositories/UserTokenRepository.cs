using System;
using Microsoft.AspNetCore.Identity;
using QSales.DAL.Repositories.Base;
using QSales.DAL.Abstraction.Interfaces;
using QSales.Model.Context;

namespace QSales.DAL.Repositories
{
    public class UserTokenRepository : GenericEntityRepository<IdentityUserToken<Guid>>, 
                                       IUserTokenRepository
    {
        public UserTokenRepository(QSalesContext netcoreTemplateContext) : base(netcoreTemplateContext)
        {
        }
    
    }
}