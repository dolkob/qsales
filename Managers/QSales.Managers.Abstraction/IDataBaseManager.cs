﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using QSales.DAL.Abstraction.Interfaces;
using QSales.Model.Identity;

namespace QSales.Managers.Abstraction
{
    public interface IDataBaseManager : IDisposable
    {
        IUserRepository UserRepository { get; }
        IUserTokenRepository UserTokenRepository { get; }
        UserManager<QSalesUser> UserManager { get; }
        RoleManager<QSalesRole> RoleManager { get; }
        SignInManager<QSalesUser> SignInManager { get; }

        void Commit();
        Task CommitAsync();
        void RejectChanges();
    }
}
