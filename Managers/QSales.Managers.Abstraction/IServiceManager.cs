﻿using System;
using QSales.Service.Abstraction;

namespace QSales.Managers.Abstraction
{
    public interface IServiceManager : IDisposable
    {
        IAuthService AuthService { get; }
        IUserService UserService { get; }
    }
}
