﻿using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using QSales.DAL.Abstraction.Interfaces;
using QSales.DAL.Repositories;
using QSales.Managers.Abstraction;
using QSales.Model.Context;
using QSales.Model.Identity;

namespace QSales.Managers
{
    public class DataBaseManager : IDataBaseManager
    {
        private readonly QSalesContext _dbContext;
        private readonly IPrincipal _principal;
        private IPasswordHasher<QSalesUser> _passwordHasher;

        private IUserRepository _userRepository;
        private IUserTokenRepository _userTokenRepository;
        public DataBaseManager(
            QSalesContext dbContext,
            IPrincipal principal,
            UserManager<QSalesUser> userManager,
            RoleManager<QSalesRole> roleManager,
            SignInManager<QSalesUser> signInManager,
            IPasswordHasher<QSalesUser> passwordHasher
            )
        {
            _dbContext = dbContext;
            _principal = principal;

            UserManager = userManager;
            RoleManager = roleManager;
            SignInManager = signInManager;
            _passwordHasher = passwordHasher;
        }

        public IUserRepository UserRepository
        {
            get
            {
                _userRepository = _userRepository ?? new UserRepository(UserManager, _dbContext, _principal);
                return _userRepository;
            }
        }

        public IUserTokenRepository UserTokenRepository
        {
            get
            {
                _userTokenRepository = _userTokenRepository ?? new UserTokenRepository(_dbContext);
                return _userTokenRepository;
            }
        }

        public UserManager<QSalesUser> UserManager { get; }

        public RoleManager<QSalesRole> RoleManager { get; }

        public SignInManager<QSalesUser> SignInManager { get; }

        public void Commit()
        {
            _dbContext.SaveChanges();
        }

        public Task CommitAsync()
        {
            return _dbContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            _userRepository?.Dispose();
            _userTokenRepository?.Dispose();

            _dbContext.Dispose();
        }
        public void RejectChanges()
        {
            foreach (var entry in _dbContext.ChangeTracker.Entries()
                  .Where(e => e.State != EntityState.Unchanged))
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    case EntityState.Modified:
                    case EntityState.Deleted:
                        entry.Reload();
                        break;
                }
            }
        }

    }
}
