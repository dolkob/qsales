﻿using AutoMapper;
using QSales.Managers.Abstraction;
using QSales.Service;
using QSales.Service.Abstraction;

namespace QSales.Managers
{
    public class ServiceManager : IServiceManager
    {
        private IAuthService _authService;
        private IUserService _userService;

        private IDataBaseManager _dataBaseManager { get; set; }
        private readonly IMapper _mapper;

        public IAuthService AuthService
        {
            get
            {
                _authService = _authService ?? new AuthService(_dataBaseManager, _mapper);
                return _authService ?? new AuthService(_dataBaseManager, _mapper);
            }
        }

        public IUserService UserService
        {
            get
            {
                _userService = _userService ?? new UserService(_dataBaseManager, _mapper);
                return _userService ?? new UserService(_dataBaseManager, _mapper);
            }
        }
        public ServiceManager(IDataBaseManager dataBaseManager, IMapper mapper)
        {
            _dataBaseManager = dataBaseManager;
            _mapper = mapper;
        }

        public void Dispose()
        {
            _authService?.Dispose();
            _userService?.Dispose();
        }

    }
}
