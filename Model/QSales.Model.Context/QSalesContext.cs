﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using QSales.Model.Identity;
using QSales.Model.Companies;

namespace QSales.Model.Context
{
    public class QSalesContext : IdentityDbContext<QSalesUser, QSalesRole, int>
    {
        public QSalesContext(DbContextOptions<QSalesContext> options)
            : base(options)
        {
        }

        public DbSet<Company> Companies {get; set;}
        public DbSet<CompanyUser> CompanyUsers {get; set;}

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Company>()
                .HasMany(c => c.CompanyUsers).WithOne(m => m.Company).OnDelete(DeleteBehavior.Restrict);
                
            base.OnModelCreating(builder);
        }

    }
}
