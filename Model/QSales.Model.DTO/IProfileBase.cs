﻿using AutoMapper;

namespace QSales.Model.DTO
{
    public interface IProfileBase
    {
        IProfileExpression Configure(IProfileExpression config);
    }
}
