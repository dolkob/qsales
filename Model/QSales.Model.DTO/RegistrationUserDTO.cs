﻿using System.ComponentModel.DataAnnotations;
using AutoMapper;
using QSales.Model.Identity;

namespace QSales.Model.DTO
{
    public class RegistrationUserDTO : IProfileBase
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password don't match")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Full Name")]
        public string UserName { get; set; }

        public IProfileExpression Configure(IProfileExpression config)
        {
            config.CreateMap<RegistrationUserDTO, QSalesUser>()
                .ForMember(dto => dto.UserName, dto => dto.MapFrom(d => d.UserName))
                .ForMember(dto => dto.Email, dto => dto.MapFrom(d => d.Email))
                .ReverseMap();

            return config;
        }

    }
}
