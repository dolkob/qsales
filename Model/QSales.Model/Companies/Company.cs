using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using QSales.Core.Model.Abstraction.Entities;

namespace QSales.Model.Companies
{
    [Table("Companies")]
    public class Company : NameCoreEntity
    {
        public string Address {get; set;}
        public string ConnectionString {get; set;}

        public virtual ICollection<CompanyUser> CompanyUsers {get; set;}
    }
}