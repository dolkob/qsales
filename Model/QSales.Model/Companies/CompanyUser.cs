using System.ComponentModel.DataAnnotations.Schema;
using QSales.Core.Model.Abstraction.Entities;
using QSales.Model.Identity;

namespace QSales.Model.Companies
{
    public class CompanyUser : TrackableEntity<int,int>
    {
        public int CompanyId {get; set;}
        public int QSalesUserId {get; set;}

        [ForeignKey(nameof(CompanyId))]
        public virtual Company Company {get; set;}
        [ForeignKey(nameof(QSalesUserId))]
        public virtual QSalesUser QSalesUser {get; set;}
    }
}