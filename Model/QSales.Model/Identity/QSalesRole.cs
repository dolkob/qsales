﻿using System;
using Microsoft.AspNetCore.Identity;
using QSales.Core.Model.Abstraction.Interfaces;

namespace QSales.Model.Identity
{
    public class QSalesRole : IdentityRole<int>, IStatable<int>
    {
        public bool IsDeleted { get; set; }
    }
}
