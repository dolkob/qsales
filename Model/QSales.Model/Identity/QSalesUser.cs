﻿using System;
using Microsoft.AspNetCore.Identity;
using QSales.Core.Model.Abstraction.Interfaces;

namespace QSales.Model.Identity
{
    public class QSalesUser : IdentityUser<int>, ITrackable<int, int>
    {
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public bool IsDeleted { get; set; }
    }
}
