﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using AutoMapper;
using QSales.Managers.Abstraction;
using QSales.Model.DTO;
using QSalesWebAPI.Controllers.Base;

namespace QSalesWebAPI.Controllers
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    public class AuthController : BaseController
    {
        private readonly IConfiguration _configuration;
        private readonly IServiceManager _serviceManager;
        private ILogger<AuthController> _logger;


        public AuthController(
            IMapper mapper,
            IDataBaseManager dataBaseManager,
            IServiceManager serviceManager,
            IConfiguration configuration,
            ILogger<AuthController> logger) : base(mapper, dataBaseManager)
        {
            _logger = logger;
            _configuration = configuration;
            _serviceManager = serviceManager;
        }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] RegistrationUserDTO model)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            try
            {
                var identityResult = await _serviceManager.AuthService.Register(model);
                DataBaseManager.Commit();

                if (identityResult.Succeeded)
                {
                    return Ok();
                }

                return BadRequest();
            }
            catch (Exception exception)
            {
                return BadRequest(exception);
            }
        }

        [HttpPost("CreateToken")]
        public async Task<IActionResult> CreateToken([FromBody] LoginUserDTO model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var user = await _serviceManager.UserService.GetUserByEmail(model.Email);
            if (user == null)
                throw new Exception("Invalid Password or Email");

            try
            {
                var token = await _serviceManager.AuthService.CreateToken(user, model.Password, _configuration);
                var newRefreshToken = await _serviceManager.AuthService
                    .CreateRefreshToken(Guid.Parse(token.Id), _configuration);
                var refreshToken = new JwtSecurityTokenHandler().WriteToken(newRefreshToken);

                await _serviceManager.AuthService.UpdateRefreshTokens(Guid.Parse(token.Id), refreshToken);

                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    refreshToken
                });
            }
            catch(Exception exception)
            {
                return BadRequest(exception);
            }
        }
    }
}
