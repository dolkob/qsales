﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using QSales.Managers.Abstraction;

namespace QSalesWebAPI.Controllers.Base
{
    public abstract class BaseController : Controller
    {
        protected readonly IMapper Mapper;
        protected readonly IDataBaseManager DataBaseManager;

        public BaseController(IMapper mapper, IDataBaseManager dataBaseManager)
        {
            Mapper = mapper;
            DataBaseManager = dataBaseManager;
        }

    }
}
