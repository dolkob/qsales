using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using QSales.Model.Context;

public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<QSalesContext>
{
    public QSalesContext CreateDbContext(string[] args)
    {
        IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json")
            .Build();
        var builder = new DbContextOptionsBuilder<QSalesContext>();
        var connectionString = configuration.GetConnectionString("QSalesContext");
        builder.UseNpgsql(connectionString);
        return new QSalesContext(builder.Options);
    }
}