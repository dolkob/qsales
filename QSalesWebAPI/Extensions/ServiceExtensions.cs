﻿using System.Security.Principal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using QSales.Managers;
using QSales.Managers.Abstraction;
using QSales.Model.Identity;
using QSales.DAL.Abstraction.Interfaces;
using QSales.DAL.Repositories;

namespace QSalesWebAPI.Extensions
{
    public static class ServiceExtensions
    {
        public static IServiceCollection RegisterServices(
            this IServiceCollection services)
        {
            services.AddTransient<UserManager<QSalesUser>>();
            services.AddTransient<RoleManager<QSalesRole>>();

            services.AddScoped<IDataBaseManager, DataBaseManager>();
            services.AddScoped<IServiceManager, ServiceManager>();

            services.AddTransient<IUserRepository, UserRepository>();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<IPrincipal>(
                provider => provider.GetService<IHttpContextAccessor>().HttpContext.User);

            return services;
        }

    }
}
