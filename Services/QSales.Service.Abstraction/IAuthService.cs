﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using QSales.Model.DTO;
using QSales.Model.Identity;

namespace QSales.Service.Abstraction
{
    public interface IAuthService : IDisposable
    {
        Task<IdentityResult> Register(RegistrationUserDTO model);
        Task<JwtSecurityToken> CreateToken(QSalesUser model, string password, IConfiguration configuration);
        Task<JwtSecurityToken> CreateRefreshToken(Guid userId, IConfiguration configuration);
        Task<IdentityUserToken<Guid>> UpdateRefreshTokens(Guid userId, string refreshToken);
    }
}
