using System;
using System.Threading.Tasks;
using QSales.Model.Identity;

namespace QSales.Service.Abstraction
{
    public interface IUserService : IDisposable
    {
         Task<QSalesUser> GetUserByEmail(string email);
    }
}