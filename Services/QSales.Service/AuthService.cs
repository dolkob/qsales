﻿using System;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using QSales.Managers.Abstraction;
using QSales.Model.DTO;
using QSales.Model.Identity;
using QSales.Service.Abstraction;

namespace QSales.Service
{
    public class AuthService : BaseService, IAuthService
    {
        public AuthService(IDataBaseManager dataBaseManager, IMapper mapper) : base(dataBaseManager, mapper)
        {
        }

        public async Task<IdentityResult> Register(RegistrationUserDTO model)
        {
            var user = await _dataBaseManager.UserManager.FindByEmailAsync(model.Email);
            if (user != null)
            {
                return IdentityResult.Failed(new IdentityError
                {
                    Code = "ExistingEmail",
                    Description = "User with such email exist"
                });
            }
            var newUser = _mapper.Map<QSalesUser>(model);
            var result = await _dataBaseManager.UserManager.CreateAsync(newUser, model.Password);

            return result;
        }

        public async Task<JwtSecurityToken> CreateToken(QSalesUser user, string password, IConfiguration configuration)
        {
            var isLegalUser = await _dataBaseManager.UserManager.CheckPasswordAsync(user, password);
            if (!isLegalUser)
                return null;
                
            var claims = new Claim[]
            {
                new Claim(JwtRegisteredClaimNames.Jti, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, new DateTimeOffset(DateTime.Now).ToUniversalTime().ToUnixTimeSeconds().ToString(), ClaimValueTypes.Integer64)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddMinutes(Convert.ToDouble(configuration["Jwt:ExpireMinutes"]));

            var token = new JwtSecurityToken(
                claims: claims,
                expires: expires,
                signingCredentials: creds
            );

            return token;
        }

        public async Task<JwtSecurityToken> CreateRefreshToken(Guid userId, IConfiguration configuration)
        {
            var user = await _dataBaseManager.UserManager.FindByIdAsync(userId.ToString());

            var claims = new Claim[]
            {
                new Claim(JwtRegisteredClaimNames.Jti, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, new DateTimeOffset(DateTime.Now).ToUniversalTime().ToUnixTimeSeconds().ToString(), ClaimValueTypes.Integer64)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(Convert.ToDouble(configuration["Jwt:RefreshTokenExpireDays"]));

            var refreshToken = new JwtSecurityToken(
                claims: claims,
                expires: expires,
                signingCredentials: creds
            );

            return refreshToken;
            
        }

        public async Task<IdentityUserToken<Guid>> UpdateRefreshTokens(Guid userId, string refreshToken)
        {
            var existenRefreshToken = await _dataBaseManager
                          .UserTokenRepository
                          .Get()
                          .FirstOrDefaultAsync(ut => ut.UserId == userId);

            IdentityUserToken<Guid> newRefreshToken = null;

            if (existenRefreshToken != null)
            {
                existenRefreshToken.Value = refreshToken;
                await _dataBaseManager.UserTokenRepository.UpdateAsync(existenRefreshToken);
            }
            else
            {
                newRefreshToken = await _dataBaseManager.UserTokenRepository.CreateAsync(new IdentityUserToken<Guid>
                {
                    LoginProvider = "asp_net_provider",
                    Name = "refresh_token",
                    Value = refreshToken,
                    UserId = userId
                });
            }

            await _dataBaseManager.CommitAsync();

            return newRefreshToken;
        }
    }
}
