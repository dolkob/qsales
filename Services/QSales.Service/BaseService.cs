﻿using System;
using AutoMapper;
using QSales.Managers.Abstraction;

namespace QSales.Service
{
    public class BaseService : IDisposable
    {
        protected readonly IDataBaseManager _dataBaseManager;
        protected readonly IMapper _mapper;

        protected BaseService(IDataBaseManager dataBaseManager, IMapper mapper)
        {
            _dataBaseManager = dataBaseManager;
            _mapper = mapper;
        }

        public void Dispose()
        {
            _dataBaseManager.Dispose();
        }
    }
}
