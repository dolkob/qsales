using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using QSales.Managers.Abstraction;
using QSales.Model.Identity;
using QSales.Service.Abstraction;

namespace QSales.Service
{
    public class UserService : BaseService, IUserService
    {
        public UserService(IDataBaseManager dataBaseManager, IMapper mapper) : base(dataBaseManager, mapper)
        {
        }
    
        public async Task<QSalesUser> GetUserByEmail(string email)
        {
            return await _dataBaseManager.UserManager.Users.SingleAsync(x => x.Email == email);
        }
    }
}